﻿using Diploma_EduForecastWPF.AdditionalClasses;
using Diploma_EduForecastWPF.Views;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;

namespace Diploma_EduForecastWPF.ViewModels
{
    public class TaskViewModel : BaseViewModel
    {

        public ObservableCollection<Task> Tasks { get; private set; }

        private Type selectedType;
        private Difficulty selectedDifficulty;
        private ObservableCollection<Subject> selectedSubjects;

        private Demand selectedDemand;
        private ObservableCollection<Demand> selectedDemands;
        public ObservableCollection<Demand> AllDemandsExceptSelected { get; set; }



        public ObservableCollection<Demand> GetDemands { get; }
        

        public ObservableCollection<Type> GetTypes { get; }
        public ObservableCollection<Difficulty> GetDifficulties { get; }
        public ObservableCollection<Subject> GetSubjects { get; }

        private Type newType;
        private Difficulty newDifficulty;

        public ICommand AddNewTypeToDBCommand { get; private set; } //переписать обработчики окон на команды
        public ICommand AddNewDifficultyToDBCommand { get; private set; } //переписать обработчики окон на команды

        private Event Ev;

        public TaskViewModel(Event E)
        {
            GetTypes = DBWorker.GetAllTaskTypes();
            GetDifficulties = DBWorker.GetAllTaskDifficulties();
            GetSubjects = DBWorker.GetAllSubjects();
            GetDemands = DBWorker.GetAllDemands();

            //Устанавливаем значения по умолчанию
            if (GetTypes.Count != 0) SelectedType = GetTypes[0];
            if (GetDifficulties.Count != 0) SelectedDifficulty = GetDifficulties[0];
            if (GetSubjects.Count != 0) SelectedSubject = GetSubjects[0];

            EventTasks = DBWorker.GetTasksByEvent(E);
            GetAllTasks = DBWorker.GetTasksExceptEvent(E);

            AllDemandsExceptSelected = DBWorker.GetAllDemands(); //поправить на except
            SelectedDemands = new ObservableCollection<Demand>(); //поправить на соответствующие таску

            Ev = E;
        }
                                                                  //Добавить добавление новой компетенции в БД

        public Type SelectedType
        {
            get { return selectedType; }
            set
            {
                selectedType = value;
                OnPropertyChanged("SelectedType");
            }
        }


        public Difficulty SelectedDifficulty
        {
            get { return selectedDifficulty; }
            set
            {
                selectedDifficulty = value;
                OnPropertyChanged("SelectedDifficulty");
            }
        }

        public Demand SelectedDemand
        {
            get { return selectedDemand; }
            set
            {
                selectedDemand = value;
                OnPropertyChanged("SelectedDemand");
            }
        }

        public ObservableCollection<Demand> SelectedDemands
        {
            get { return selectedDemands; }
            set
            {
                selectedDemands = value;
                OnPropertyChanged("SelectedDemands");
            }
        }

        public ObservableCollection<Subject> SelectedSubjects
        {
            get { return selectedSubjects; }
            set
            {
                selectedSubjects = value;
                OnPropertyChanged("SelectedSubjects");
            }
        }

        private List<Task> eventTasks;
        public List<Task> EventTasks
        {
            get { return eventTasks; }
            set
            {
                eventTasks = value;
                OnPropertyChanged("EventTasks");
            }
        }

        private List<Task> allTasks;
        public List<Task> GetAllTasks
        {
            get { return allTasks; }
            set
            {
                allTasks = value;
                OnPropertyChanged("GetAllTasks");
            }
        }

        private Task selectedTask;
        public Task SelectedTask
        {
            get { return selectedTask; }
            set
            {
                selectedTask = value;
                OnPropertyChanged("SelectedTask");
            }
        }

        private Subject selectedSubject;
        public Subject SelectedSubject
        {
            get { return selectedSubject; }
            set
            {
                selectedSubject = value;
                OnPropertyChanged("SelectedSubject");
            }
        }

        #region RELAY COMMANDS
        private RelayCommand addDemandToTask;
        /// <summary>
        /// Команда добавления компетенции в список задачи
        /// </summary>
        public RelayCommand AddDemandToTask
        {
            get
            {
                return addDemandToTask ??
                  (addDemandToTask = new RelayCommand(obj =>
                  {
                      Demand workDemand = (Demand)obj;

                      //Если правая часть не содержит наш деманд, то добавляем
                      if(!AllDemandsExceptSelected.Contains(workDemand)) AllDemandsExceptSelected.Add(workDemand);

                      //Если левая часть содержит наш деманд, то удаляем
                      if(SelectedDemands.Contains(workDemand)) SelectedDemands.Remove(workDemand);

                      OnPropertyChanged("AllDemandsExceptSelected");
                      OnPropertyChanged("SelectedDemands");
                  }));
            }
        }

        private RelayCommand removeDemandFromTask;
        /// <summary>
        /// Команда удаления компетенции из списка задачи
        /// </summary>
        public RelayCommand RemoveDemandFromTask
        {
            get
            {
                return removeDemandFromTask ??
                  (removeDemandFromTask = new RelayCommand(obj =>
                  {
                      Demand workDemand = (Demand)obj;

                      //Если правая часть содержит наш деманд, то удаляем
                      if (AllDemandsExceptSelected.Contains(workDemand)) AllDemandsExceptSelected.Remove(workDemand);

                      //Если левая часть не содержит наш деманд, то добавляем
                      if (!SelectedDemands.Contains(workDemand)) SelectedDemands.Add(workDemand);

                      OnPropertyChanged("AllDemandsExceptSelected");
                      OnPropertyChanged("SelectedDemands");
                  }));
            }
        }
        
        private RelayCommand addTask;
        public RelayCommand AddTask
        {
            get
            {
                return addTask ??
                  (addTask = new RelayCommand(obj =>
                  {
                      GetAllTasks.Remove((Task)obj);
                      EventTasks.Add((Task)obj);
                  }));
            }
        }

        private RelayCommand removeTask;
        public RelayCommand RemoveTask
        {
            get
            {
                return removeTask ??
                  (removeTask = new RelayCommand(obj =>
                  {
                      GetAllTasks.Add((Task)obj);
                      EventTasks.Remove((Task)obj);
                  }));
            }
        }

        private RelayCommand saveChangesToDB;
        /// <summary>
        /// Команда сохранения изменения в базу данных
        /// </summary>
        public RelayCommand SaveChangesToDB
        {
            get
            {
                return saveChangesToDB ??
                  (saveChangesToDB = new RelayCommand(obj =>
                  {
                      //Здесь сохранить изменения для текущего таска в основном окне изменения.
                      Task t = new Task
                      {
                          Demands = SelectedDemands,
                          Difficulty = SelectedDifficulty,
                          Subject = SelectedSubject,
                          Type = SelectedType,
                      };

                      DBWorker.AddTask(t);

                  }));
            }

        }

        private RelayCommand newTypeWindowShow;
        /// <summary>
        /// Команда открытия окна создания нового типа задания
        /// </summary>
        public RelayCommand NewTypeWindowShow
        {
            get
            {
                return newTypeWindowShow ??
                    (newTypeWindowShow = new RelayCommand(obj =>
                    {
                        var typeWindow = new NewTypeWindow();
                        typeWindow.ShowDialog();
                    }));
            }
        }

        private RelayCommand newDifficultyWindowShow;
        /// <summary>
        /// Команда открытия окна создания нового типа сложности задания
        /// </summary>
        public RelayCommand NewDifficultyWindowShow
        {
            get
            {
                return newDifficultyWindowShow ??
                    (newDifficultyWindowShow = new RelayCommand(obj =>
                    {
                        var difficultyWindow = new NewDifficultyWindow();
                        difficultyWindow.ShowDialog();
                    }));
            }
        }

        private RelayCommand newDemandWindowShow;
        /// <summary>
        /// Команда открытия окна выбора и создания компетенций
        /// </summary>
        public RelayCommand NewDemandWindowShow
        {
            get
            {
                return newDemandWindowShow ??
                    (newDemandWindowShow = new RelayCommand(obj =>
                    {
                        var demandsWindow = new AddDemandsToTaskWindow(this);
                        demandsWindow.ShowDialog();
                    }));
            }
        }
#endregion

        private void AddNewTypeToDBMethod()
        {
        }

    }
}
