﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using unvell.ReoGrid;
using System.Data;
using Microsoft.Win32;
using Diploma_EduForecastWPF.AdditionalClasses;

namespace Diploma_EduForecastWPF.ViewModels
{
    public class DataCell
    {
        public string Name { get; set; }
        public string Range { get; set; }
    }

    class ProtocolViewModel : BaseViewModel
    {
        private List<DataCell> ListData;
        private DataCell selectedData;
        

        public ProtocolViewModel()
        {
            ListData = new List<DataCell>
            {
                new DataCell { Name = "Дата", Range = ""},
                new DataCell { Name = "Название", Range = "" },
                new DataCell{ Name = "Предмет", Range = "" },
                new DataCell{ Name = "Код ОО", Range = "" },
                new DataCell{ Name = "Задания с кратким ответом", Range = "" },
                new DataCell{ Name = "Задания с развернутым ответом", Range = "" },
                new DataCell{ Name = "Первичный балл", Range = "" },
                new DataCell{ Name = "Вторичный балл", Range = "1" }
            };

            fileToLoadToGrid = "";

            //application = new Application();
            //selectedFile = application.Workbooks.Open(@"C:\1.xlsx");
            //Text =  new List<string> { "asdas", "asdasdassa"};
            //IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

        }

        public DataCell SelectedData
        {
            get { return selectedData; }
            set
            {
                selectedData = value;
                OnPropertyChanged("SelectedData");
            }
        }

        public List<DataCell> GetListData
        {
            get { return ListData; }
            set
            {
                ListData = value;
                OnPropertyChanged("GetListData");
            }
        }

        public void UpdateListData(string cell)
        {
            SelectedData.Range = cell;
        }

        public Workbook SelectedFile
        {
            get
            {
                return null; 
            }
            set
            {
                //selectedFile = value;
                OnPropertyChanged("SelectedFile");
            }
        }

        private string fileToLoadToGrid;
        /// <summary>
        /// Файл, который будет загружаться в ReoGridControl
        /// </summary>
        public string FileLoadToString
        {
            get
            {
                return fileToLoadToGrid;
            }

            set
            {
                fileToLoadToGrid = value;
                OnPropertyChanged("FileLoadToString");
            }
        }

#region RELAY COMMANDS
        private RelayCommand openFile;
        /// <summary>
        /// Команда открытия диалога выбора файла
        /// </summary>
        public RelayCommand OpenProtocolFile
        {
            get
            {
                return openFile ??
                  (openFile = new RelayCommand(obj =>
                  {
                      //selectedFile = application.Workbooks.Open((string)obj); //obj - путь к файлу
                      OpenFileDialog dialog = new OpenFileDialog();
                      if (dialog.ShowDialog() == true)
                      {
                          FileLoadToString = dialog.FileName;
                      }
                  }));
            }
        }

        private RelayCommand processProtocol;
        /// <summary>
        /// Команда нажатия на кнопку обработки протокола
        /// </summary>
        public RelayCommand ProcessProtocol
        {
            get
            {
                return processProtocol ??
                    (processProtocol = new RelayCommand(obj =>
                    {

                    }));
            }
        }

        //Пока не будет использоваться. Позже придумаем, как сделать
        private RelayCommand importSelectedFileToGrid;
        public RelayCommand ImportSelectedFileToGrid
        {
            get
            {
                return importSelectedFileToGrid ??
                  (importSelectedFileToGrid = new RelayCommand(obj =>
                  {
                     // (ReoGridControl)obj); //obj - путь к файлу
                  }));
            }
        }
#endregion


    }
}
