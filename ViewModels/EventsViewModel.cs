﻿using Diploma_EduForecastWPF.AdditionalClasses;
using Diploma_EduForecastWPF.Views;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Diploma_EduForecastWPF.ViewModels
{
    public class EventsViewModel : BaseViewModel
    {
        private Event selectedEvent;
        public List<TaskSubjectForView> EventTasks{ get; set; }
        //private ObservableCollection<Demand> selectedEventDemands;
        public ObservableCollection<Event> Events { get; set; }

        public EventsViewModel()
        {
            this.Events = DBWorker.GetAllEvents();
            this.EventTasks = new List<TaskSubjectForView>();
        }

        public Event SelectedEvent
        {
            get { return selectedEvent; }
            set
            {
                selectedEvent = value;
                //EventTasks = (List<TaskSubjectForView>)DBWorker.GetEventTasks(selectedEvent); //такое преобразование делать нельзя

                EventTasks.Clear();
                foreach(Task task in DBWorker.GetEventTasks(selectedEvent))
                {
                    EventTasks.Add(new TaskSubjectForView(task));
                }

                OnPropertyChanged("EventTasks");
                OnPropertyChanged("SelectedEvent");
            }
        }


#region RELAY COMMANDS
        private RelayCommand addNewEventToList;
        /// <summary>
        /// Команда добавления нового события (мероприятия)
        /// </summary>
        public RelayCommand AddNewEventToList
        {
            get
            {
                return addNewEventToList ??
                  (addNewEventToList = new RelayCommand(obj =>
                  {
                      Events.Add(new Event { EventID = Events.Count > 0 ? Events.Last().EventID + 1 : 0, Name = "Новое мероприятие", Year = System.DateTime.Now });
                      selectedEvent = Events.Last();
                  }));
            }
        }

        private RelayCommand addNewTaskToEvent;
        /// <summary>
        /// Добавление нового задания в событие (мероприятие)
        /// </summary>
        public RelayCommand AddNewTaskToEvent
        {
            get
            {
                return addNewTaskToEvent ??
                    (addNewTaskToEvent = new RelayCommand(obj =>
                    {
                        var addNewTaskWindow = new NewTaskWindow(new TaskViewModel(selectedEvent));
                        addNewTaskWindow.ShowDialog();
                        OnPropertyChanged("EventTasks");
                    }));
            }
        }

        private RelayCommand saveSelectedEventToDB;
        /// <summary>
        /// Сохранение текущего события (мероприятия) в базу данных
        /// </summary>
        public RelayCommand SaveSelectedEventToDB
        {
            get
            {
                return saveSelectedEventToDB ??
                  (saveSelectedEventToDB = new RelayCommand(obj =>
                  {
                      /*Здесь сохраняются только изменения имени и года.*/
                      DBWorker.ChangeOrSaveNewEvent((Event)obj);
                  }));
            }
        }

        private RelayCommand openProtocolCommand;
        /// <summary>
        /// Открытие редактирования протокола
        /// </summary>
        public RelayCommand OpenProtocolCommand
        {
            get
            {
                return openProtocolCommand ??
                  (openProtocolCommand = new RelayCommand(obj =>
                  {
                      ProtocolWindow window = new ProtocolWindow(SelectedEvent);
                      window.ShowDialog();
                  }));
            }
        }
#endregion

    }
}
