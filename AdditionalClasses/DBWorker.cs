﻿using Diploma_EduForecastWPF.AdditionalClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Diploma_EduForecastWPF
{
    public class DBWorker
    {
        /*Дальше будут все методы для работы с БД. CRUD и т.д.*/
        public static ObservableCollection<Subject> GetAllSubjects()
        {
            using (var db = new   DBContext())
            {
                return new ObservableCollection<Subject>(db.Subjects);
            }
        }

        //public static List<TaskSubjectForView> GetEventTasks(Event E)
        //{
        //    using (var db = new   DBContext())
        //    {
        //        var list = new List<TaskSubjectForView>();
        //        var c = db.TaskTemplates.Where(s => s.TestTemplate.EventID == E.EventID).ToList();
        //        foreach (var tt in c)
        //        {
        //            Task task = db.Tasks.Where(t => t.TaskID == tt.TaskID).SingleOrDefault();
        //            Subject subject = db.Subjects.Where(s => s.SubjectID == task.SubjectID).SingleOrDefault();
        //            Type type = db.Types.Where(x => x.TypeID == task.TypeID).SingleOrDefault();
        //            Difficulty difficulty = db.Difficulties.Where(x => x.DifficultyID == task.DifficultyID).SingleOrDefault();
        //            //Добавить demands
        //            //Demand demand = db.Demands.Where(x => x.DemandID == task.Demand).SingleOrDefault();
        //            list.Add(new TaskSubjectForView { Number = (int)tt.Number, MaxBall = (int)task.MaxBall, Subject = subject.Name, Difficulty = difficulty.Name, Type = type.Name });
        //        }

        //        return list;
        //    }
        //}

        public static List<Task> GetTasksByEvent(Event E)
        {
            using (var db = new  DBContext())
            {
                var list = new List<Task>();
                var c = db.TaskTemplates.Where(s => s.TestTemplate.EventID == E.EventID).ToList();
                foreach (var tt in c)
                {
                    Task task = db.Tasks.Where(t => t.TaskID == tt.TaskID).SingleOrDefault();
                    list.Add(task);
                }

                return list;
            }
        }

        public static List<Task> GetTasksExceptEvent(Event E)
        {
            using (var db = new   DBContext())
            {
                var list = new List<Task>();
                var c = db.TaskTemplates.Where(s => s.TestTemplate.EventID != E.EventID).ToList();
                foreach (var tt in c)
                {
                    Task task = db.Tasks.Where(t => t.TaskID == tt.TaskID).SingleOrDefault();
                    list.Add(task);
                }

                return list;
            }
        }

        public static ObservableCollection<TaskSubjectForView> GetAllTasksSFV()
        {
            using (var db = new   DBContext())
            {
                ObservableCollection<TaskSubjectForView> list = new ObservableCollection<TaskSubjectForView>();
                foreach (var task in db.Tasks)
                {
                    Subject subject = db.Subjects.Where(s => s.SubjectID == task.SubjectID).SingleOrDefault();
                    Type type = db.Types.Where(x => x.TypeID == task.TypeID).SingleOrDefault();
                    Difficulty difficulty = db.Difficulties.Where(x => x.DifficultyID == task.DifficultyID).SingleOrDefault();
                    //Добавить demands
                    //Demand demand = db.Demands.Where(x => x.DemandID == task.Demand).SingleOrDefault();
                    //list.Add(new TaskSubjectForView { MaxBall = (int)task.MaxBall, Subject = subject.Name, Difficulty = difficulty.Name, Type = type.Name });
                    list.Add(new TaskSubjectForView(task));
                }

                return list;
            }
        }

        public static bool TryAddProtocolTemplate(Event ev)
        {
            //try
            //{
                using (var db = new   DBContext())
                {
                    ProtocolTemplate protocolTemplate = new ProtocolTemplate();

                    protocolTemplate.TestTemplate = db.TestTemplates.FirstOrDefault(t => t.EventID == ev.EventID);
                    db.ProtocolTemplates.Add(protocolTemplate);
                    db.SaveChanges();
                }
                return true;
            //}
            //catch
            //{
            //    return false;
            //}
        }

        public static ObservableCollection<Event> GetAllEvents()
        {
            using (var db = new   DBContext())
            {
                return new ObservableCollection<Event>(db.Events);
            }
        }

        public static ObservableCollection<Task> GetAllTasks()
        {
            using (var db = new   DBContext())
            {
                return new ObservableCollection<Task>(db.Tasks);
            }
        }

        internal static void ChangeOrSaveNewEvent(Event obj)
        {
            using (var db = new   DBContext())
            {
                if (db.Events.Find(obj.EventID) != null)
                {
                    /*Пока что только год и имя.*/
                    db.Events.Find(obj.EventID).Name = obj.Name;
                    db.Events.Find(obj.EventID).Year = obj.Year;
                }
                else
                {
                    db.Events.Add(obj);
                    TryAddTestTemplate(obj);
                }

                db.SaveChanges();
            }
        }

        public static ObservableCollection<Demand> GetAllDemands()
        {
            using (var db = new   DBContext())
            {
                return new ObservableCollection<Demand>(db.Demands);
            }
        }

        public static ObservableCollection<Difficulty> GetAllTaskDifficulties()
        {
            using (var db = new   DBContext())
            {
                return new ObservableCollection<Difficulty>(db.Difficulties);
            }
        }

        public static ObservableCollection<Type> GetAllTaskTypes()
        {
            using (var db = new   DBContext())
            {
                return new ObservableCollection<Type>(db.Types);
            }
        }

        /*Переписать, т.к. может быть много предметов. Использовать List, например.*/
        public static bool AddTask(Task task)
        {
            try
            {
                using (var db = new   DBContext())
                {
                    //task.Difficulty = difficulty;
                    //task.Subject = subject;
                    //task.Type = type;
                    //task.MaxBall = maxBall;
                    //Добавить деманд

                    db.Tasks.Add(task);
                    return true;
                }
            }
            catch
            {
                return false; //если не удалось нормально добавить
            }
        }
        public static bool AddTask(Difficulty difficulty, Subject subject, Type type, int maxBall, List<Demand> demands)
        {
            //try
            //{
                using (var db = new   DBContext())
                {
                    Task task = new Task();
                    task.Difficulty = difficulty;
                    task.DifficultyID = difficulty.DifficultyID;
                    task.Subject = subject;
                    task.SubjectID = subject.SubjectID;
                    task.Type = type;
                    task.TypeID = type.TypeID;
                    task.MaxBall = maxBall;
                    foreach (Demand d in demands)
                    {
                        task.Demands.Add(d);
                    }
                    //task.Demands = demands;
                        
                    db.Tasks.Add(task);
                    db.SaveChanges();
                    return true;
                }
            //}
            //catch
            //{
            //    return false; //если не удалось нормально добавить
            //}
        }


        public static bool AddType(string s)
        {
            try
            {
                using (var db = new   DBContext())
                {
                    var type = new Type(); //здесь можно добавлять через {} наверное, а еще нужно не забыть обновлять ID автоматически
                    type.Name = s;
                    db.Types.Add(type);
                    db.SaveChanges();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool AddDifficulty(string s)
        {
            try
            {
                using (var db = new   DBContext())
                {
                    var difficulty = new Difficulty(); //здесь можно добавлять через {} наверное, а еще нужно не забыть обновлять ID автоматически 
                    difficulty.Name = s;
                    db.Difficulties.Add(difficulty);
                    db.SaveChanges();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool AddCell(Event _event, string cellName, string value)
        {
            try
            {
                using (var db = new   DBContext())
                {
                    var cell = new Cell();
                    //var cell = new Cell { ProtocolTemplateID = _event.TestTemplates.First().ProtocolTemplate.ProtocolTemplateID, CellID = db.Cells.Last().CellID++, Name = cellName, ProtocolTemplate = _event.TestTemplates.First().ProtocolTemplate }; //здесь можно добавлять через {} наверное, а еще нужно не забыть обновлять ID автоматически 
                    cell.ProtocolTemplate = _event.TestTemplates.FirstOrDefault().ProtocolTemplate;
                    cell.ProtocolTemplateID = _event.TestTemplates.FirstOrDefault().ProtocolTemplate.ProtocolTemplateID;
                    cell.Name = cellName;
                    cell.Format = value; //пока вместо формата храню value
                    db.Cells.Add(cell);
                    db.SaveChanges();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool IsTestTemplateExists(Event @event)
        {
            using (var db = new   DBContext())
            {
                
                if (db.Events.FirstOrDefault(e => e.EventID == @event.EventID).TestTemplates.Count != 0)
                    return true;
                else
                    return false;
            }
        }

        public static bool TryAddTestTemplate(Event _event) //, string _subject
        {
            try
            {
                using (var db = new   DBContext())
                {
                    TestTemplate t = new TestTemplate();
                    //t.TestTemplateID = db.TestTemplates.Last().TestTemplateID++; //поправить!
                    t.Event = _event;
                    //t.Subject = db.Subjects.First(s => s.Name == _subject);
                    //ProtocolTemplate protocolTemplate = new ProtocolTemplate { TestTemplateID = t.TestTemplateID };
                    //db.ProtocolTemplates.Add(protocolTemplate);
                    db.SaveChanges();
                    t.ProtocolTemplate = db.ProtocolTemplates.Last();
                    db.TestTemplates.Add(t);
                    db.SaveChanges();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }


        public bool AddStudentResult(OU ou, DateTime date, string className, TestTemplate tt, List<int> tasksResults)
        {
            try
            {
                using (var _context = new DBContext())
                {
                    var test = new Test
                    {
                        //TestTemplateId = tt.TestTemplateID,
                    };

                    var taskTests = tt.TaskTemplates
                        .Where(tasktemplate =>
                        //???
                        tasktemplate.Number != null && tasksResults.Contains(tasktemplate.Number.Value))
                        .Select(tasktemp => new TaskTest
                        {
                            Task = tasktemp.Task,
                            Test = test
                        });

                    test.TaskTests = new List<TaskTest>(taskTests);

                    var student = new OUStudent
                    {
                        OU = ou,
                        Date = date,
                        Class = className,
                        Tests = new List<Test> { test }
                    };
                    _context.OUStudents.Add(student);
                    _context.SaveChanges();
                }
                return true;

            }
            catch
            {
                return false;
            }
        }

        static public IEnumerable<Task> GetEventTasks(Event e)
        {
            return e.TestTemplates.SelectMany(tt => tt.TaskTemplates).Select(taskTemp => taskTemp.Task);
        }

        private IEnumerable<Demand> GetTaskDemand(Task t)
        {
            return t.Demands;
        }

        private Subject GetTaskSubject(Task t)
        {
            using (var _context = new DBContext())
            {
                return _context.Subjects.FirstOrDefault(s => s.SubjectID == t.SubjectID);
            }
        }

        private double GetAvg(OU ou, Task task)
        {
            var allStudentsTests = ou.OUStudents.SelectMany(stud => stud.Tests);

            var allStudentsTestsResults =
                allStudentsTests.SelectMany(t => t.TaskTests).Where(tt => tt.TaskID == task.TaskID);
            return allStudentsTestsResults.Where(tt => tt.Ball != null).Average(tt => tt.Ball.Value);
        }

        public void AddDemand(string _name, Demand _Demand2)
        {
            using (var _context = new DBContext())
            {
                Demand d = new Demand();
                d.Name = _name;
                d.Demand2 = _Demand2;

                _context.Demands.Add(d);

                _context.SaveChanges();
            }
        }

        /*

        public void AddDifficulty(string _name)
        {
            using (var _context = new DBContext())
            {
                Difficulty d = new Difficulty();
                d.Name = _name;

                _context.Difficulties.Add(d);

                _context.SaveChanges();
            }
        }
        

        public void AddType(string _name)
        {
            using (var _context = new DBContext())
            {
                Type t = new Type();
                t.Name = _name;
                _context.Types.Add(t);
                _context.SaveChanges();
            }
        }
        */

        public void AddSubject(string _name, int _code, int _parentSubjectID)
        {
            using (var _context = new DBContext())
            {
                Subject s = new Subject();
                s.Name = _name;
                s.Code = _code;
                s.ParentSubjectID = _parentSubjectID;

                _context.Subjects.Add(s);

                _context.SaveChanges();
            }
        }

        public void AddOU(int _SchoolKindID, int _SchoolPropertyID, int _MOUO_ID, string _Name, SchoolKind _sk, SchoolProperty _sp, MOUO _mouo)
        {
            using (var _context = new DBContext())
            {
                OU o = new OU();
                o.SchoolKindID = _SchoolKindID;
                o.SchoolPropertyID = _SchoolPropertyID;
                o.MOUO_ID = _MOUO_ID;
                o.Name = _Name;
                o.SchoolKind = _sk;
                o.SchoolProperty = _sp;
                o.MOUO = _mouo;

                _context.OUs.Add(o);

                _context.SaveChanges();
            }
        }

        public void AddOU(string _Name, SchoolKind _sk, SchoolProperty _sp, MOUO _mouo)
        {
            using (var _context = new DBContext())
            {
                OU o = new OU();
                o.Name = _Name;
                o.SchoolKind = _sk;
                o.SchoolProperty = _sp;
                o.MOUO = _mouo;

                _context.OUs.Add(o);

                _context.SaveChanges();
            }
        }

        public void AddSchoolKind(int _Code, string _Name, int _SchoolTypeID, OU _OU, SchoolType _type)
        {
            using (var _context = new DBContext())
            {
                SchoolKind sk = new SchoolKind();
                sk.Code = _Code;
                sk.Name = _Name;
                sk.SchoolTypeID = _SchoolTypeID;
                sk.OU = _OU;
                sk.SchoolType = _type;

                _context.SchoolKinds.Add(sk);

                _context.SaveChanges();
            }
        }

        public void AddSchoolType(int _Code, string _Name)
        {
            using (var _context = new DBContext())
            {
                SchoolType st = new SchoolType();
                st.Code = _Code;
                st.Name = _Name;

                _context.SchoolTypes.Add(st);

                _context.SaveChanges();
            }
        }

        public void AddSchoolProperties(int _Code, string _Name, OU _OU)
        {
            using (var _context = new DBContext())
            {
                SchoolProperty sp = new SchoolProperty();
                sp.Code = _Code;
                sp.Name = _Name;
                sp.OU = _OU;

                _context.SchoolProperties.Add(sp);

                _context.SaveChanges();
            }
        }

        public void AddMOUO(int _Code, string _Name, int _DistrictID, District _D)
        {
            using (var _context = new DBContext())
            {
                MOUO mo = new MOUO();
                mo.Code = _Code;
                mo.Name = _Name;
                mo.DistrictID = _DistrictID;
                mo.District = _D;

                _context.MOUOs.Add(mo);

                _context.SaveChanges();
            }
        }

        public void AddDistrict(int _Code, string _Name)
        {
            using (var _context = new DBContext())
            {
                District d = new District();
                d.Code = _Code;
                d.Name = _Name;

                _context.Districts.Add(d);

                _context.SaveChanges();
            }
        }

        public void AddTask(Difficulty _D, Type _t, Subject _s, int m, TaskTemplate _tt)
        {
            using (var _context = new DBContext())
            {
                Task t = new Task();
                t.Difficulty = _D;
                t.Type = _t;
                t.Subject = _s;
                //t.Number = m; //надо передать номер
                t.TaskTemplates.Add(_tt);

                _context.Tasks.Add(t);

                _context.SaveChanges();
            }
        }

        public void AddProtocolTemplate(string _Name, string _Date, string _SubjectCode, string _OU_Code, string _ShortAnswers, string _FirstBall, string _SecondBall, int _TestTemplateID, int _LongAnswers, TestTemplate _t)
        {
            using (var _context = new DBContext())
            {
                ProtocolTemplate pt = new ProtocolTemplate();
                pt.Name = _Name;
                pt.Date = _Date;
                pt.SubjectCode = _SubjectCode;
                pt.OU_Code = _OU_Code;
                pt.ShortAnswers = _ShortAnswers;
                pt.FirstBall = _FirstBall;
                pt.SecondBall = _SecondBall;
                pt.TestTemplateID = _TestTemplateID;
                pt.LongAnswers = _LongAnswers;
                pt.TestTemplate = _t;

                _context.ProtocolTemplates.Add(pt);

                _context.SaveChanges();
            }
        }

        public void AddTestTemplate(Event _E)
        {
            using (var _context = new DBContext())
            {
                TestTemplate t = new TestTemplate();
                t.Event = _E;
                _context.TestTemplates.Add(t);
                _context.SaveChanges();
            }
        }
    }
}
