﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diploma_EduForecastWPF.AdditionalClasses
{
    /// <summary>
    /// Класс-обертка для отображения задач во вьюшке
    /// </summary>
    public class TaskSubjectForView
    {
        protected Task childTask;

        /// <summary>
        /// Номер задачи
        /// </summary>
        public int Number
        {
            get
            {
                return childTask.TaskID;
            }

        }

        /// <summary>
        /// Название задачи
        /// </summary>
        public string Subject
        {
            get
            {
                return childTask.Subject.Name;
            }

        }

        /// <summary>
        /// Требования к задаче
        /// </summary>
        public string Demand
        {
            get
            {
                string AllDemands = "";
                foreach(Demand demand in childTask.Demands)
                {
                    AllDemands = string.Concat(demand.Name, "");
                }
                return AllDemands;
            }
        }

        /// <summary>
        /// Сложность задачи
        /// </summary>
        public string Difficulty
        {
            get
            {
                return childTask.Difficulty.Name;
            }
        }

        /// <summary>
        /// Тип задачи
        /// </summary>
        public string Type
        {
            get
            {
                return childTask.Type.Name;
            }
        }

        /// <summary>
        /// Максимальное количество баллов за задачу
        /// </summary>
        public int MaxBall
        {
            get
            {
                return childTask.MaxBall == null ? 0 : (int)childTask.MaxBall;
            }
        }

        public TaskSubjectForView()
        {
        }

        public TaskSubjectForView(Task task)
        {
            childTask = task;
        }

        public TaskSubjectForView(int n, string s)
        {
            //Пока так - используем только инициализацию дочерней моделью
            //Number = n;
            //Subject = s;
        }

    }
}
