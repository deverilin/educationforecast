﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace Diploma_EduForecastWPF.AdditionalClasses
{
    public class ExcelWorker
    {
        private Excel.Application xlApp; //это можно вынести из класса
        private Excel.Workbook xlBook;
        private Excel.Worksheet xlSheet;
        private Excel.Range xlRange;
        int current_row; //excel row positions

        public ExcelWorker(string filepath)
        {
            xlApp = new Excel.Application();
            xlBook = xlApp.Workbooks.Open(@"C:\1.xlsx"); //поменять на filepath
            xlSheet = xlBook.Sheets[1];
        }

        public string GetCellValue(string cell)
        {
            return xlSheet.Rows[6].Cells[2].Value.ToString();
        }

        public System.Array GetRange(string range)
        {
            //return worksheet.Range[worksheet.Rows[6].Cells[2], worksheet.Rows[144].Cells[2]].Cells.Value;
            var s = range.Split(':');
            return (System.Array)xlSheet.get_Range("B6", "B144").Cells.Value;
        }

        public bool TryParseShortAnswerTasks(string s)
        {
            return false;
        }

        public List<TestResult> TryParseLongAnswerTasks(string s)
        {
            int N = s.Length;
            List<TestResult> res = new List<TestResult>();
            if (N > 3)
            {
                int period = N / 4;
                for (int i = 0; i < period; i++)
                {
                    int ball = Convert.ToInt32(s[4 * i] - '0');
                    int maxball = Convert.ToInt32(s[4 * i + 2] - '0');
                    TestResult tr = new TestResult(ball, maxball);
                    res.Add(tr);
                }
            }
            return res;
        }

        //public DateTime GetDateFromString(string s)
        //{
        //    return null;
        //}

        public Subject GetSubjectFromString(string s)
        {
            return null;
        }

        public void ReadFile()
        {
            //if (OpenConnection(file_in.FileName))
            if (OpenConnection(""))
            {
                List<Student> students = new List<Student>();
                var next = xlRange.Cells[current_row, 1].Value;
                while (next != null)
                {
                    int _code = Convert.ToInt32(xlRange.Cells[current_row, 2].Value);


                    /*распарсить строку "++--++"*/
                    int _temp = 0;
                    List<int> ares = new List<int>();
                    ares.Add(_temp);

                    string s = xlRange.Cells[current_row, 9].Value.ToString();
                    List<TestResult> bres = TryParseLongAnswerTasks(s);

                    int _first = Convert.ToInt32(xlRange.Cells[current_row, 10].Value);
                    int _second = Convert.ToInt32(xlRange.Cells[current_row, 11].Value);

                    Student cur = new Student(_code, ares, bres, _first, _second);
                    students.Add(cur);
                    current_row++;
                    next = xlRange.Cells[current_row, 1].Value;
                }
                //закрыть объекты эксель
                CloseConnection();
            }
        }

        private bool OpenConnection(string file_name)
        {
            if (File.Exists(file_name))
            {
                //если нужно дополнить
                //var full_path = Path.Combine(Directory.GetCurrentDirectory(), file_name);

                try
                {
                    xlApp = new Excel.Application();
                    xlBook = xlApp.Workbooks.Open(file_name, 0, false, 5, "", "", true,
                        Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", true, false, 0, true, 1, 0);
                    xlSheet = (Excel.Worksheet)xlBook.Sheets[1];
                    xlRange = xlSheet.Cells;
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("Interop.Excel Connection FAILED: " + ex.Message);
                    return false;
                }
                return true;
            }
            return false;
        }

        private void CloseConnection()
        {
            xlBook.Close(true);
            xlApp.Quit();

            ReleaseObject(xlSheet);
            ReleaseObject(xlBook);
            ReleaseObject(xlApp);
        }

        private void ReleaseObject(Object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                //MessageBox.Show("Exception while releasing object: " + ex.Message);
            }
            finally
            {
                GC.Collect();
            }
        }
    }

    class Student
    {
        public int code { set; get; }
        public List<int> AResults;
        public List<TestResult> BResults;
        public int FirstBall { set; get; }
        public int SecondBall { set; get; }

        public Student(int _code, List<int> _Ares, List<TestResult> _Bres, int _first, int _second)
        {
            code = _code;
            FirstBall = _first;
            SecondBall = _second;
            AResults = new List<int>();
            BResults = new List<TestResult>();

            int N = _Ares.Count;
            for (int i = 0; i < N; i++)
                AResults.Add(_Ares[i]);

            N = _Bres.Count;
            for (int i = 0; i < N; i++)
            {
                TestResult tr = new TestResult(_Bres[i].Ball, _Bres[i].MaxBall);
                BResults.Add(tr);
            }
        }
    }

    public class TestResult
    {
        public int Ball { set; get; }
        public int MaxBall { set; get; }

        public TestResult(int _ball, int _maxball)
        {
            Ball = _ball;
            MaxBall = _maxball;
        }
    }
}
