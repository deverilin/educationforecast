namespace Diploma_EduForecastWPF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OU")]
    public partial class OU
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OU()
        {
            OUStudents = new HashSet<OUStudent>();
        }

        [Key]
        public int OU_ID { get; set; }

        public int? SchoolKindID { get; set; }

        public int? SchoolPropertyID { get; set; }

        public int? MOUO_ID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public virtual MOUO MOUO { get; set; }

        public virtual SchoolKind SchoolKind { get; set; }

        public virtual SchoolProperty SchoolProperty { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OUStudent> OUStudents { get; set; }
    }
}
