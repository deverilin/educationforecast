namespace Diploma_EduForecastWPF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProtocolTemplate")]
    public partial class ProtocolTemplate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProtocolTemplate()
        {
            Cells = new HashSet<Cell>();
        }

        public int ProtocolTemplateID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(10)]
        public string Date { get; set; }

        [StringLength(10)]
        public string SubjectCode { get; set; }

        [StringLength(10)]
        public string OU_Code { get; set; }

        [StringLength(10)]
        public string ShortAnswers { get; set; }

        [StringLength(10)]
        public string FirstBall { get; set; }

        [StringLength(10)]
        public string SecondBall { get; set; }

        public int? TestTemplateID { get; set; }

        public int? LongAnswers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cell> Cells { get; set; }

        public virtual TestTemplate TestTemplate { get; set; }
    }
}
