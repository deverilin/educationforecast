namespace Diploma_EduForecastWPF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Cell")]
    public partial class Cell
    {
        public int CellID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Format { get; set; }

        public int? ProtocolTemplateID { get; set; }

        public virtual ProtocolTemplate ProtocolTemplate { get; set; }
    }
}
