namespace Diploma_EduForecastWPF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SchoolKind")]
    public partial class SchoolKind
    {
        public int SchoolKindID { get; set; }

        public int? Code { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public int? SchoolTypeID { get; set; }

        public virtual OU OU { get; set; }

        public virtual SchoolType SchoolType { get; set; }
    }
}
