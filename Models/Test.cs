namespace Diploma_EduForecastWPF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Test")]
    public partial class Test
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Test()
        {
            TaskTests = new HashSet<TaskTest>();
        }

        public int TestID { get; set; }

        public int? TestTemplate { get; set; }

        public int? OUStudentID { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date { get; set; }

        public int? FirstBall { get; set; }

        public int? SecondBall { get; set; }

        public int? Mark { get; set; }

        public double? PercentComplete { get; set; }

        public virtual OUStudent OUStudent { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TaskTest> TaskTests { get; set; }

        public virtual TestTemplate TestTemplate1 { get; set; }
    }
}
