namespace Diploma_EduForecastWPF
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Diploma_EduForecastWPF.Models;

    public partial class DBContext : DbContext
    {
        public DBContext()
            : base("name=DBContext")
        {
        }

        public virtual DbSet<Cell> Cells { get; set; }
        public virtual DbSet<Demand> Demands { get; set; }
        public virtual DbSet<Difficulty> Difficulties { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<MOUO> MOUOs { get; set; }
        public virtual DbSet<OU> OUs { get; set; }
        public virtual DbSet<OUStudent> OUStudents { get; set; }
        public virtual DbSet<ProtocolTemplate> ProtocolTemplates { get; set; }
        public virtual DbSet<SchoolKind> SchoolKinds { get; set; }
        public virtual DbSet<SchoolProperty> SchoolProperties { get; set; }
        public virtual DbSet<SchoolType> SchoolTypes { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<Subject> Subjects { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<TaskTemplate> TaskTemplates { get; set; }
        public virtual DbSet<TaskDemand> TaskDemands { get; set; }
        public virtual DbSet<TaskTest> TaskTests { get; set; }
        public virtual DbSet<Test> Tests { get; set; }
        public virtual DbSet<TestTemplate> TestTemplates { get; set; }
        public virtual DbSet<Type> Types { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Demand>()
                .HasMany(e => e.Demand1)
                .WithOptional(e => e.Demand2)
                .HasForeignKey(e => e.ParentDemand);

            modelBuilder.Entity<Demand>()
                .HasMany(e => e.Tasks)
                .WithMany(e => e.Demands)
                .Map(m => m.ToTable("TaskDemands").MapLeftKey("DemandID").MapRightKey("TaskID"));

            modelBuilder.Entity<OUStudent>()
                .Property(e => e.Class)
                .IsFixedLength();

            modelBuilder.Entity<ProtocolTemplate>()
                .Property(e => e.Date)
                .IsFixedLength();

            modelBuilder.Entity<ProtocolTemplate>()
                .Property(e => e.SubjectCode)
                .IsFixedLength();

            modelBuilder.Entity<ProtocolTemplate>()
                .Property(e => e.OU_Code)
                .IsFixedLength();

            modelBuilder.Entity<ProtocolTemplate>()
                .Property(e => e.ShortAnswers)
                .IsFixedLength();

            modelBuilder.Entity<ProtocolTemplate>()
                .Property(e => e.FirstBall)
                .IsFixedLength();

            modelBuilder.Entity<ProtocolTemplate>()
                .Property(e => e.SecondBall)
                .IsFixedLength();

            modelBuilder.Entity<SchoolKind>()
                .HasOptional(e => e.OU)
                .WithRequired(e => e.SchoolKind);

            modelBuilder.Entity<SchoolProperty>()
                .HasOptional(e => e.OU)
                .WithRequired(e => e.SchoolProperty);

            modelBuilder.Entity<Subject>()
                .HasMany(e => e.Subject1)
                .WithOptional(e => e.Subject2)
                .HasForeignKey(e => e.ParentSubjectID);

            modelBuilder.Entity<Task>()
                .HasMany(e => e.TaskTemplates)
                .WithRequired(e => e.Task)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Task>()
                .HasMany(e => e.TaskTests)
                .WithRequired(e => e.Task)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Test>()
                .HasMany(e => e.TaskTests)
                .WithRequired(e => e.Test)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TestTemplate>()
                .HasOptional(e => e.ProtocolTemplate)
                .WithRequired(e => e.TestTemplate);

            modelBuilder.Entity<TestTemplate>()
                .HasMany(e => e.TaskTemplates)
                .WithRequired(e => e.TestTemplate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TestTemplate>()
                .HasMany(e => e.Tests)
                .WithOptional(e => e.TestTemplate1)
                .HasForeignKey(e => e.TestTemplate);
        }
    }
}
