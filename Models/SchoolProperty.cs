namespace Diploma_EduForecastWPF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SchoolProperty")]
    public partial class SchoolProperty
    {
        public int SchoolPropertyID { get; set; }

        public int? Code { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public virtual OU OU { get; set; }
    }
}
