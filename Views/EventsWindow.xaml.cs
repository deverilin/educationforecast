﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Diploma_EduForecastWPF.ViewModels;
using Diploma_EduForecastWPF.Views;

namespace Diploma_EduForecastWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class EventsWindow : Window
    {
        object dc;
        public EventsWindow()
        {
            InitializeComponent();
            DataContext = new EventsViewModel();
            dc = DataContext;
        }

/*
 * Пока не используется
       private void btnChangeEventsTasks_Click(object sender, RoutedEventArgs e)
       {

       }

private void btnChangeEventsTasks_Click(object sender, RoutedEventArgs e)
{
  var a = ((EventsViewModel)dc).SelectedEvent;
  TasksWindow t = new TasksWindow(a);

  //NewTaskWindow n = new NewTaskWindow(a);
  t.Show();
}

private void btnSaveNewEvent_Click(object sender, RoutedEventArgs e)
{
  //TasksWindow t = new TasksWindow();
 // t.ShowDialog();
}

private void btnCreateProtocolTemplate_Click(object sender, RoutedEventArgs e)
{
  ProtocolWindow window = new ProtocolWindow(((EventsViewModel)dc).SelectedEvent);
  window.Show();
}
*/
    }
}
