﻿using Diploma_EduForecastWPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Diploma_EduForecastWPF.Views
{
    /// <summary>
    /// Логика взаимодействия для TasksViewModel.xaml
    /// </summary>
    public partial class TasksWindow : Window
    {
        object dc;
        Event E;
        public TasksWindow(Event _E)
        {
            InitializeComponent();
            E = _E;
            DataContext = new TaskViewModel(E);
        }

        private void btnCreateNewTask_Click(object sender, RoutedEventArgs e)
        {
            NewTaskWindow newTaskWindow = new NewTaskWindow((TaskViewModel)DataContext);
            newTaskWindow.Show();
            DataContext = new TaskViewModel(E);
        }
    }
}
