﻿using Diploma_EduForecastWPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Diploma_EduForecastWPF.Views
{
    /// <summary>
    /// Логика взаимодействия для NewTaskWindow.xaml
    /// </summary>
    public partial class NewTaskWindow : Window
    {
        public NewTaskWindow(TaskViewModel t)
        {
            InitializeComponent();
            DataContext = t;
        }

        /*
        private void btnNewType_Click(object sender, RoutedEventArgs e)
        {
            var newTypeWindow = new NewTypeWindow();
            newTypeWindow.ShowDialog();

            //ПОМЕНЯТЬ НА КОМАНДЫ И (ВОЗМОЖНО) ВПИСАТЬ В ОБЩУЮ ФОРМУ "НОВОЕ ЗАДАНИЕ"
        }

        private void btnNewDifficulty_Click(object sender, RoutedEventArgs e)
        {
            var window = new NewDifficultyWindow();
            window.ShowDialog();


            //ПОМЕНЯТЬ НА КОМАНДЫ И (ВОЗМОЖНО) ВПИСАТЬ В ОБЩУЮ ФОРМУ "НОВОЕ ЗАДАНИЕ"
        }
        */

        private void btnChangeDemands_Click(object sender, RoutedEventArgs e)
        {
            //AddDemandsToTaskWindow window = new AddDemandsToTaskWindow(out (TaskViewModel)DataContext);
            //window.ShowDialog();
        }

        private void btnSaveTaskToDB_Click(object sender, RoutedEventArgs e)
        {
            //this.Close();
        }

        private void clickCloseWindow(object sender, RoutedEventArgs e)
        {
            //DBWorker.AddTask((Difficulty)comboBoxDifficulty.SelectedItem, (Subject)cbSubject.SelectedItem, (Type)comboBoxType.SelectedItem, Convert.ToInt32(tbMaxBall.Text), (((TaskViewModel)DataContext).SelectedDemands).ToList());
            //Используем только для закрытия формы
            this.Close();
        }
    }
}
