﻿using Diploma_EduForecastWPF.ViewModels;
using Diploma_EduForecastWPF.AdditionalClasses;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Diploma_EduForecastWPF.Views
{
    /// <summary>
    /// Логика взаимодействия для ProtocolWindow.xaml
    /// </summary>
    public partial class ProtocolWindow : Window
    {

        ProtocolViewModel protocolViewModel;
        Event mainEvent;
        public ProtocolWindow(Event ev) 
        {
            InitializeComponent();
            mainEvent = ev;
            protocolViewModel = new ProtocolViewModel();
            DataContext = protocolViewModel;

            using (var db = new EduDBContext())
            {
                if (db.ProtocolTemplates.FirstOrDefault(p => p.TestTemplateID == db.TestTemplates.FirstOrDefault(t => t.EventID == ev.EventID).TestTemplateID) == null)
                {
                    DBWorker.TryAddProtocolTemplate(ev);
                }
            }
                
        }

        private void chooseFile_Click(object sender, RoutedEventArgs e)
        {
            /*
            Microsoft.Win32.OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                grid.Load(dialog.FileName);
                grid.CurrentWorksheet.SelectionRangeChanging += (s, es) => tbRange.Text = grid.CurrentWorksheet.SelectionRange.ToString();
            }
            */

            var dt = (ProtocolViewModel)DataContext;

            dt.OpenProtocolFile.Execute(null);
            grid.Load(dt.FileLoadToString);
            grid.CurrentWorksheet.SelectionRangeChanging += (s, es) => tbRange.Text = grid.CurrentWorksheet.SelectionRange.ToString();
        }

        private void btnAddDataCell_Click(object sender, RoutedEventArgs e)
        {
            DBWorker.AddCell(mainEvent, ((ProtocolViewModel)DataContext).SelectedData.Name, ((ProtocolViewModel)DataContext).SelectedData.Range);
        }
    }
}
